package animalland;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SoundDao;

/**
 * Servlet implementation class SoundDelete
 */
@WebServlet("/SoundDelete")
public class SoundDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SoundDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
			return;
		}



		request.setCharacterEncoding("UTF-8");
		int soundId = Integer.parseInt(request.getParameter("soundId"));


		try {
			SoundDao sd = new SoundDao();
			int result = sd.DeleteSound(soundId);

			if(result == 1) {
				 RequestDispatcher dispatcher = request.getRequestDispatcher("mypage");
				dispatcher.forward(request, response);
				return;
			}else{

				RequestDispatcher dispatcher = request.getRequestDispatcher("mypage");
				dispatcher.forward(request, response);
				return;
			}


			}catch(Exception e) {
				e.printStackTrace();
				response.sendRedirect("mypage");
			}
	}


}
