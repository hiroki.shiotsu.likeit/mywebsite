package animalland;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import dao.AnimalDao;

/**
 * Servlet implementation class AnimalDelete
 */
@WebServlet("/AnimalDelete")
public class AnimalDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnimalDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
			return;
		}

		request.setCharacterEncoding("UTF-8");
		int animalId = Integer.parseInt(request.getParameter("animalId"));

		try {
			AnimalDao ad = new AnimalDao();
			int result = ad.DeleteAnimal(animalId);

			if(result == 1) {
				 RequestDispatcher dispatcher = request.getRequestDispatcher("CreateAnimal");
				dispatcher.forward(request, response);
				return;
			}else{

				AnimalDao AD = new AnimalDao();
				List<AnimalDataBeans> animalList = AD.allAnimal();


				request.setAttribute("animalList", animalList);
				request.setAttribute("errMsg", "失敗しました");

				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createAnimal.jsp");
					dispatcher.forward(request, response);
					return;
			}


			}catch(Exception e) {
				e.printStackTrace();
				response.sendRedirect("mypage");
			}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

}
