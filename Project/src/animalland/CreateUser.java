package animalland;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import dao.AnimalDao;
import dao.UserDao;

/**
 * Servlet implementation class CreateUser
 */
@WebServlet("/CreateUser")
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//sessionが存在する場合タイムラインに遷移
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck != null) {
			response.sendRedirect("TimeLine");
			return;
		}



		try {
		AnimalDao AD = new AnimalDao();
		List<AnimalDataBeans> animalList = AD.allAnimal();


		request.setAttribute("animalList", animalList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newAnimal.jsp");
		dispatcher.forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Index");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		//登録処理してindexに遷移する処理
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		int animalId = Integer.parseInt(request.getParameter("animalId"));


		String password = null;
		if(loginId.isEmpty()) {
			loginId = null;
		}

		if(password1.equals(password2) ) {
			password = password1;
		}

		if(password1.isEmpty() || password2.isEmpty()) {
			 request.setAttribute("loginId", loginId);
			 request.setAttribute("userName", userName);
			 request.setAttribute("birthDate", birthDate);
			 request.setAttribute("errMsg", "入力された内容は正しくありません");

			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newAnimal.jsp");
			dispatcher.forward(request, response);
			return;

		}

		UserDao userDao = new UserDao();
		int SQLResult =  userDao.CreateUser(loginId, password, userName, birthDate, animalId);
		 if(SQLResult == 0) {
			 request.setAttribute("loginId", loginId);
			 request.setAttribute("userName", userName);
			 request.setAttribute("birthDate", birthDate);
			 request.setAttribute("errMsg", "入力された内容は正しくありません");

			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newAnimal.jsp");
			dispatcher.forward(request, response);
			return;
			}

		 response.sendRedirect("Index");
		}catch(SQLException e) {
			e.printStackTrace();
			response.sendRedirect("CreateUser");
		}


	}

}
