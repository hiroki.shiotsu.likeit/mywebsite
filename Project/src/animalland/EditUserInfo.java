package animalland;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import beans.UserDataBeans;
import dao.AnimalDao;
import dao.UserDao;

/**
 * Servlet implementation class EditUserInfo
 */
@WebServlet("/EditUserInfo")
public class EditUserInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//ssesion切れちぇっく

			Object sessionCheck = session.getAttribute("userId");
			if(sessionCheck == null) {
				response.sendRedirect("Index");
						return;
			}


			//ユーザー情報の取得
			int userId = (int) session.getAttribute("userId");

			UserDataBeans udb = UserDao.getUserInfo(userId);

			//動物情報の取得
			AnimalDao AD = new AnimalDao();
			List<AnimalDataBeans> animalList = AD.allAnimal();


			request.setAttribute("animalList", animalList);
			request.setAttribute("userInfo",udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editUserInfo.jsp");
			dispatcher.forward(request, response);
			}catch(Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Login");
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//登録処理してmypageに遷移する処理
			HttpSession session = request.getSession(false);
			int userId = (int) session.getAttribute("userId");
			request.setCharacterEncoding("UTF-8");

			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");
			String userName = request.getParameter("userName");
			String birthDate = request.getParameter("birthDate");
			int animalId = Integer.parseInt(request.getParameter("animalId"));


			String password = null;


			if(password1.equals(password2) ) {
				password = password1;
			}

			if(password1.isEmpty() || password2.isEmpty()) {
				UserDataBeans udb = UserDao.getUserInfo(userId);
				AnimalDao AD = new AnimalDao();
				List<AnimalDataBeans> animalList = AD.allAnimal();
				request.setAttribute("animalList", animalList);
				request.setAttribute("userInfo",udb);
				 request.setAttribute("errMsg", "パスワードが空です");

				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editUserInfo.jsp");
				dispatcher.forward(request, response);
				return;

			}

			UserDao userDao = new UserDao();
			int SQLResult =  userDao.editUserInfo(password, userName, birthDate, animalId, userId);
			 if(SQLResult == 0) {
				 UserDataBeans udb = UserDao.getUserInfo(userId);
				 AnimalDao AD = new AnimalDao();
				List<AnimalDataBeans> animalList = AD.allAnimal();
				request.setAttribute("animalList", animalList);
				request.setAttribute("userInfo",udb);
				 request.setAttribute("errMsg", "入力された内容は正しくありません");

				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editUserInfo.jsp");
				dispatcher.forward(request, response);
				return;
				}

			 response.sendRedirect("mypage");
			}catch(SQLException e) {
				e.printStackTrace();
				response.sendRedirect("mypage");
			}
	}

}
