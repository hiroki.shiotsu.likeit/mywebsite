package animalland;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SoundDao;

/**
 * Servlet implementation class Sound
 */
@WebServlet("/Sound")
public class Sound extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sound() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//session切れチェック
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
			return;
		}

		try {
			//jspから値を受け取りセット
			request.setCharacterEncoding("UTF-8");

			int userId = Integer.parseInt(request.getParameter("userId"));
			int flg = Integer.parseInt(request.getParameter("flg"));
			String soundContent = request.getParameter("soundContent");

			//soundContentが空の場合
			if(soundContent.equals("")) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "1文字以上入力してください");
				// 同ページにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("mypage");
				dispatcher.forward(request, response);
				return;
			}


			//DAOつぶやきを保存する
			SoundDao sd = new SoundDao();
			int SQLResult = sd.NewSound(userId, soundContent);
			//同じページに遷移・・・したい
			if(SQLResult == 1) {

				if(flg == 1) {
					response.sendRedirect("TimeLine");
				}else {
					response.sendRedirect("mypage");
				}


			}else {
				request.setAttribute("errMsg", "1文字以上入力してください");

				if(flg == 1) {
					RequestDispatcher dispatcher = request.getRequestDispatcher("TimeLine");
					dispatcher.forward(request, response);
				}else {
					RequestDispatcher dispatcher = request.getRequestDispatcher("mypage");
					dispatcher.forward(request, response);
				}


				return;
			}
			}catch(Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("timeline.jsp");
			}

	}




}
