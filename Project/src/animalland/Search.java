package animalland;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import beans.SoundInfoBeans;
import beans.UserDataBeans;
import dao.AnimalDao;
import dao.SoundDao;
import dao.UserDao;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		//sessionCheck
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
			return;
		}

		//検索ワード取得
		request.setCharacterEncoding("UTF-8");
		String searchWord = request.getParameter("searchWord");

		try {
		//ユーザー情報の取得
		int userId = (int) session.getAttribute("userId");
		UserDataBeans udb = UserDao.getUserInfo(userId);
		AnimalDataBeans ab = AnimalDao.getAnimal(udb.getAnimalId());

		//ツイートの検索
		SoundDao SD = new SoundDao();
		List<SoundInfoBeans> userSoundList = SD.SearchSound(userId,searchWord);
		//



		//遷移,
		request.setAttribute("searchWord",searchWord);
		request.setAttribute("USL", userSoundList);
		request.setAttribute("animalInfo",ab);
		request.setAttribute("userId", userId);
		request.setAttribute("userInfo",udb);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline.jsp");
		dispatcher.forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Login");
		}
	}

}
