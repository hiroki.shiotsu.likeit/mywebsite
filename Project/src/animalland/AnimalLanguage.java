package animalland;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import beans.SoundInfoBeans;
import beans.UserDataBeans;
import dao.AnimalDao;
import dao.SoundDao;
import dao.UserDao;

/**
 * Servlet implementation class AnimalLanguage
 */
@WebServlet("/AnimalLanguage")
public class AnimalLanguage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnimalLanguage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ツイート取得＆遷移しなおす
		HttpSession session = request.getSession();

			try {
				//セッション切れチェック
				Object sessionCheck = session.getAttribute("userId");
				if(sessionCheck == null) {
					response.sendRedirect("Index");
					return;
				}


				//ユーザー情報の取得

				int userId = (int) session.getAttribute("userId");
				request.setCharacterEncoding("UTF-8");
				String searchWord = request.getParameter("searchWord");
				int flg = Integer.parseInt(request.getParameter("flg"));

				UserDataBeans udb = UserDao.getUserInfo(userId);
				int animalId = udb.getAnimalId();
				AnimalDataBeans ab = AnimalDao.getAnimal(udb.getAnimalId());




				request.setAttribute("animalInfo",ab);
				request.setAttribute("userId", userId);
				request.setAttribute("userInfo",udb);



				if(flg == 1) {
					//mypage
					SoundDao SD = new SoundDao();
					List<SoundInfoBeans> userSoundList = SD.userAnimalSound(userId);
					int UNOT = SD.UserNumOfTimesSound(userId);
					request.setAttribute("USL", userSoundList);
					request.setAttribute("UNOT", UNOT);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myPage.jsp");
					dispatcher.forward(request, response);

					return;
				}else if(flg == 2) {
					//timeline
					SoundDao SD = new SoundDao();
					List<SoundInfoBeans> userSoundList = SD.AllAnimalSound(userId);
					request.setAttribute("USL", userSoundList);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline.jsp");
					dispatcher.forward(request, response);
				}else if(flg == 3) {
					//すでに検索済み
					SoundDao SD = new SoundDao();
					List<SoundInfoBeans> userSoundList = SD.SearchAnimalSound(userId,searchWord);
					request.setAttribute("USL", userSoundList);
					request.setAttribute("searchWord", searchWord);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline.jsp");
					dispatcher.forward(request, response);

				}else {

				}



		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Login");
		}






	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
