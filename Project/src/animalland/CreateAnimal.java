package animalland;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import dao.AnimalDao;

/**
 * Servlet implementation class CreateAnimal
 */
@WebServlet("/CreateAnimal")
public class CreateAnimal extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAnimal() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();
		//セッション切れチェック
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
			return;
		}

		//userIdチェック
		int userId = (int) session.getAttribute("userId");
		if(userId != 1) {
			response.sendRedirect("mypage");
			return;
		}

		try {
			AnimalDao AD = new AnimalDao();
			List<AnimalDataBeans> animalList = AD.allAnimal();


			request.setAttribute("animalList", animalList);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createAnimal.jsp");
			dispatcher.forward(request, response);

		}catch(Exception e) {
			e.printStackTrace();

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//登録処理してindexに遷移する処理
			request.setCharacterEncoding("UTF-8");

			String animalName = request.getParameter("animalName");
			String animalImg = request.getParameter("animalImg");
			String animalLanguage = request.getParameter("animalLanguage");
			String animalSound = request.getParameter("animalSound");


			AnimalDao AD = new AnimalDao();
			int SQLResult = AD.CreateAnimal(animalName, animalImg, animalLanguage, animalSound);
			 if(SQLResult == 0) {
				 request.setAttribute("animalName", animalName);
				 request.setAttribute("animalImg", animalImg);
				 request.setAttribute("animalLanguage", animalLanguage);
				 request.setAttribute("animalSound", animalSound);
				 request.setAttribute("errMsg", "入力された内容は正しくありません");

				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createAnimal.jsp");
				dispatcher.forward(request, response);
				return;
				}

			 response.sendRedirect("CreateAnimal");
			}catch(SQLException e) {
				e.printStackTrace();
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createAnimal.jsp");
				dispatcher.forward(request, response);
			}

	}

}
