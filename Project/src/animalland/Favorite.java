package animalland;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.FavoriteDao;

/**
 * Servlet implementation class Favorite
 */
@WebServlet("/Favorite")
public class Favorite extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Favorite() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			//ユーザー情報の取得 sessioncheck
			Object sessionCheck = session.getAttribute("userId");
			if(sessionCheck == null) {
				response.sendRedirect("Index");
						return;
			}


			int userId = (int) session.getAttribute("userId");
			request.setCharacterEncoding("UTF-8");
			int soundId = Integer.parseInt(request.getParameter("soundId"));
			int flg = Integer.parseInt(request.getParameter("flg"));

			FavoriteDao favoriteDao = new FavoriteDao();

			int result = 0;
			if(flg == 2 || flg == 4) {
				result = favoriteDao.DeleteFavorite(userId, soundId);
			}else {
				result = favoriteDao.AddFavorite(userId, soundId);
			}


			if(flg == 1 || flg == 2){
				response.sendRedirect("TimeLine");

			}else {
				response.sendRedirect("mypage");
			}



			}catch(Exception e) {
				e.printStackTrace();
				response.sendRedirect("mypage");
			}




	}



}
