package animalland;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class EditProfile
 */
@WebServlet("/EditProfile")
public class EditProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//sessionからユーザーIDの取得
		try {
		HttpSession session = request.getSession();

		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
					return;
		}

		int userId = (int) session.getAttribute("userId");

		//入力内容セット
		request.setCharacterEncoding("UTF-8");
		String userName = request.getParameter("userName");
		String uI = request.getParameter("uI");

		UserDao ud = new UserDao();
		int SQLResult =  ud.editMypageUser(userName, uI, userId);
		if(SQLResult == 0) {
			 request.setAttribute("userName", userName);
			 request.setAttribute("uI", uI);
			 request.setAttribute("errMsg", "入力された内容は正しくありません");
		}

		 response.sendRedirect("mypage");
		}catch(SQLException e) {
			e.printStackTrace();
			response.sendRedirect("mypage");
		}
	}

}
