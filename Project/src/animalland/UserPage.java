package animalland;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AnimalDataBeans;
import beans.SoundInfoBeans;
import beans.UserDataBeans;
import dao.AnimalDao;
import dao.SoundDao;
import dao.UserDao;

/**
 * Servlet implementation class UserPage
 */
@WebServlet("/UserPage")
public class UserPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		//sessionCheck
		Object sessionCheck = session.getAttribute("userId");
		if(sessionCheck == null) {
			response.sendRedirect("Index");
			return;
		}
		request.setCharacterEncoding("UTF-8");
		//他の人
		int userId1 = Integer.parseInt(request.getParameter("userId1"));
		//自分
		int userId2 = (int) session.getAttribute("userId");

		if(userId1 == userId2) {
			response.sendRedirect("mypage");
			return;
		}

		try {
		//別ユーザーの情報の取得
		UserDataBeans udb1 = UserDao.getUserInfo(userId1);
		AnimalDataBeans ab1 = AnimalDao.getAnimal(udb1.getAnimalId());

		//ツイートの取得
		SoundDao SD = new SoundDao();
		List<SoundInfoBeans> userSoundList = SD.userSound(userId1);
		int UNOT = SD.UserNumOfTimesSound(userId1);
		//自分の情報の取得
		UserDataBeans udb2 = UserDao.getUserInfo(userId2);
		AnimalDataBeans ab2 = AnimalDao.getAnimal(udb2.getAnimalId());




		//遷移
		request.setAttribute("UNOT", UNOT);
		request.setAttribute("USL", userSoundList);
		request.setAttribute("animalInfo1",ab1);
		request.setAttribute("animalInfo2",ab2);
		request.setAttribute("userId1", userId1);
		request.setAttribute("userId", userId2);
		request.setAttribute("userInfo1",udb1);
		request.setAttribute("userInfo2",udb2);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/anotherUser.jsp");
		dispatcher.forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Login");
		}
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
