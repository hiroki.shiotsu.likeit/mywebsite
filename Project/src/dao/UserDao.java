package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;


public class UserDao {

	public String passEncrypt(String password) {
		//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			return result;
		}


	// userIdを探す
	public int getUserId(String loginId, String password) throws SQLException {

		Connection conn = null;
		PreparedStatement st = null;
		String pass = this.passEncrypt(password);
		try {
			conn = DBManager.getConnection();

			st = conn.prepareStatement("SELECT * FROM user WHERE login_id = ? and password = ?");
			st.setString(1, loginId);
			st.setString(2, pass);

			ResultSet rs = st.executeQuery();



			while (!rs.next()) {
				return 0;
			}

			int userId = rs.getInt("user_id");

			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	//ユーザIDを利用してユーザの情報を全て取得する。 mypageで使用

	public static UserDataBeans getUserInfo(int userId) throws SQLException {
		UserDataBeans  udb = new UserDataBeans();
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();

			st = conn.prepareStatement("SELECT * FROM user WHERE user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("password"));
				udb.setUserName(rs.getString("user_name"));
				udb.setBirthDate(rs.getDate("birth_date"));
				udb.setAnimalId(rs.getInt("animal_id"));
				udb.setUserIntroduction(rs.getString("user_introduction"));
			}


			System.out.println("searching userInfo  has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return udb;
	}


	//ユーザーの新規登録用DAO CreateUserで使用

	public int CreateUser(String loginId,String password,String userName,String birthDate, int animalId) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;
		String pass = this.passEncrypt(password);
		try {

   		 conn = DBManager.getConnection();

   		 String sql = "INSERT INTO user (login_id,password,user_name,birth_date,animal_id) VALUES (?,?,?,?,?)";
		 pStmt = conn.prepareStatement(sql);

		 pStmt.setString(1, loginId);
         pStmt.setString(2,pass);
         pStmt.setString(3,userName);
         pStmt.setString(4,birthDate);
         pStmt.setInt(5,animalId);

         int result = pStmt.executeUpdate();

		 if(result > 0) {
			 System.out.println("Success");
			 return success;
		 }else {
			return failed;
		 }

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}

	}

	//mypageからユーザー情報を更新するためのDao
	public int editMypageUser(String userName,String userIntroduction ,int userId) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;
		try {

   		 conn = DBManager.getConnection();

   		 String sql = "UPDATE user SET user_name = ? , user_introduction = ? WHERE user_id = ?";
		 pStmt = conn.prepareStatement(sql);

		 pStmt.setString(1, userName);
         pStmt.setString(2,userIntroduction);
         pStmt.setInt(3,userId);


         int result = pStmt.executeUpdate();

		 if(result > 0) {
			 System.out.println("Success");
			 return success;
		 }else {
			return failed;
		 }

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}
	}

	//ユーザー詳細編集
	public int editUserInfo(String password,String userName,String birthDate,int animalId,int userId) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;
		String pass= this.passEncrypt(password);
		try {

   		 conn = DBManager.getConnection();

   		 String sql = "UPDATE user SET password = ? , user_name = ?,birth_date = ?,animal_id = ? WHERE user_id = ?";
		 pStmt = conn.prepareStatement(sql);

		 pStmt.setString(1, pass);
         pStmt.setString(2,userName);
         pStmt.setString(3,birthDate);
         pStmt.setInt(4,animalId);
         pStmt.setInt(5, userId);


         int result = pStmt.executeUpdate();

		 if(result > 0) {
			 System.out.println("Success");
			 return success;
		 }else {
			return failed;
		 }

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}
	}

	//削除

	public int DeleteUser(int userId)throws SQLException{
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;
		try {

	   		 conn = DBManager.getConnection();

	   		 String sql = "DELETE FROM user WHERE user_id = ?";
			 pStmt = conn.prepareStatement(sql);

			 pStmt.setInt(1, userId);


	         int result = pStmt.executeUpdate();

			 if(result > 0) {
				 System.out.println("Success");
				 return success;
			 }else {
				return failed;
			 }

			}catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (conn != null) {
					conn.close();

				}
			}
	}



}


