package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.SoundInfoBeans;


public class SoundDao {


	//つぶやき保存用のDAO

	public int NewSound(int userId,String  soundContent) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;
		try {

   		 conn = DBManager.getConnection();

   		 String sql = "INSERT INTO sound (user_id,sound_content) VALUES (?,?)";
		 pStmt = conn.prepareStatement(sql);

		 pStmt.setInt(1, userId);
         pStmt.setString(2,soundContent);


         int result = pStmt.executeUpdate();

		 if(result > 0) {
			 System.out.println("Success");
			 return success;
		 }else {
			return failed;
		 }

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}
	}


	//Delete Sound
	public int DeleteSound(int soundId) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;
		try {

   		 conn = DBManager.getConnection();

   		 String sql = "DELETE FROM sound WHERE sound_id = ?";
		 pStmt = conn.prepareStatement(sql);

		 pStmt.setInt(1,soundId);



         int result = pStmt.executeUpdate();

		 if(result > 0) {
			 System.out.println("Success");
			 return success;
		 }else {
			return failed;
		 }

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}
	}

	//userIdに紐づくSOUND総数の取得
	public int UserNumOfTimesSound(int userId) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;

		try {

   		 conn = DBManager.getConnection();

   		 String sql = "SELECT COUNT(*) SoundT FROM sound WHERE user_id = ?;";
		 st = conn.prepareStatement(sql);

		 st.setInt(1,userId);




         ResultSet rs = st.executeQuery();


         while (!rs.next()) {
        	 return 0;
         }
         //ツイート総数を取得
         int UNOT = rs.getInt("SoundT");

         return UNOT;


		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}
	}

	//userIdに紐づくSOUNDの取得
	public List<SoundInfoBeans> userSound(int userId) throws SQLException{
		Connection conn = null;
	     List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	     PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 ("SELECT\n" +
	    					"u.user_id,\n" +
	    			 		"u.user_name,\n" +
	    			 		"a.animal_id,\n" +
	    			 		"a.animal_img,\n" +
	    			 		"a.animal_language,\n" +
	    			 		"s.sound_id,\n" +
	    			 		"s.sound_content,\n" +
	    			 		"s.sound_date,\n" +
	    			 		"f2.favorite_check,\n" +
	    			 		"COUNT(f.id) favorite_count\n" +
	    			 		"FROM \n" +
	    			 		"user u\n" +
	    			 		"INNER JOIN\n" +
	    			 		"sound s\n" +
	    			 		"ON\n" +
	    			 		"u.user_id = s.user_id\n" +
	    			 		"INNER JOIN\n" +
	    			 		"animal a\n" +
	    			 		"ON\n" +
	    			 		"a.animal_id = u.animal_id\n" +
	    			 		"LEFT OUTER JOIN \n" +
	    			 		"favorite f\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f.sound_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"(SELECT count(*)\n" +
	    			 		" favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id  ) f2\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f2.sound_id\n" +
	    			 		"where\n" +
	    			 		"u.user_id = ? \n" +
	    			 		"group by\n" +
	    			 		"s.sound_id DESC;") ;


	    	 st.setInt(1, userId);
	    	 st.setInt(2, userId);
			ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	 Date soundDate = rs.getDate("sound_date");

	        	 int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }



	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg,
	        			 animalLanguage, soundId,soundContent,soundDate,favoCheck,favoCount);


	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}


	//userのお気に入りsoundの取得
	public List<SoundInfoBeans> userFavoriteSound(int userId) throws SQLException{
		Connection conn = null;
	     List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	     PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 ("SELECT\n" +
	    			 		"u.user_id,\n" +
	    			 		"u.user_name,\n" +
	    			 		"a.animal_id,\n" +
	    			 		"a.animal_img,\n" +
	    			 		"a.animal_language,\n" +
	    			 		"s.sound_id,\n" +
	    			 		"s.sound_content,\n" +
	    			 		"s.sound_date,	    			 \n" +
	    			 		"f2.favorite_check,	    \n" +
	    			 		"COUNT(f.id) favorite_count\n" +
	    			 		"FROM\n" +
	    			 		"user u\n" +
	    			 		"INNER JOIN\n" +
	    			 		"sound s\n" +
	    			 		"ON\n" +
	    			 		"u.user_id = s.user_id\n" +
	    			 		"INNER JOIN\n" +
	    			 		"animal a\n" +
	    			 		"ON\n" +
	    			 		"u.animal_id = a.animal_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"favorite f\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f.sound_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"(SELECT count(*) favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id) f2\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f2.sound_id\n" +
	    			 		"WHERE\n" +
	    			 		"f2.favorite_check >= 1\n" +
	    			 		"GROUP BY\n" +
	    			 		"s.sound_id;\n") ;


	    	st.setInt(1, userId);
			ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	 Date soundDate = rs.getDate("sound_date");

	        	 int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }



	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg,
	        			 animalLanguage, soundId,soundContent,soundDate,favoCheck,favoCount);


	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}



	//全ての呟きを取得

	public List<SoundInfoBeans> AllSound(int userId) throws SQLException{
		Connection conn = null;
	     List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	     PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 		("SELECT\n" +
	    			 		"u.user_id,\n" +
	    			 		"u.user_name,\n" +
	    			 		"a.animal_id,\n" +
	    			 		"a.animal_img,\n" +
	    			 		"a.animal_language,\n" +
	    			 		"s.sound_id,\n" +
	    			 		"s.sound_content,\n" +
	    			 		"s.sound_date,\n" +
	    			 		"f2.favorite_check,\n" +
	    			 		"COUNT(f.id) favorite_count\n" +
	    			 		"FROM \n" +
	    			 		"user u\n" +
	    			 		"INNER JOIN\n" +
	    			 		"sound s\n" +
	    			 		"ON\n" +
	    			 		"u.user_id = s.user_id\n" +
	    			 		"INNER JOIN\n" +
	    			 		"animal a\n" +
	    			 		"ON\n" +
	    			 		"a.animal_id = u.animal_id\n" +
	    			 		"LEFT OUTER JOIN \n" +
	    			 		"favorite f\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f.sound_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"(SELECT count(*)\n" +
	    			 		" favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id  ) f2\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f2.sound_id\n" +
	    			 		"group by\n" +
	    			 		"s.sound_id DESC;");

	    	 st.setInt(1, userId);
	    	 ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	 Date soundDate = rs.getDate("sound_date");
	        	 int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }



	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg, animalLanguage,
	        				soundId,soundContent,soundDate,favoCheck,favoCount);

	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}

	//検索

	public List<SoundInfoBeans> SearchSound(int userId,String searchWord) throws SQLException{
		Connection conn = null;
	    List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	    PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 ("SELECT\n" +
	    			 		"u.user_id,\n" +
	    			 		"u.user_name,\n" +
	    			 		"a.animal_id,\n" +
	    			 		"a.animal_img,\n" +
	    			 		"a.animal_language,\n" +
	    			 		"s.sound_id,\n" +
	    			 		"s.sound_content,\n" +
	    			 		"s.sound_date,\n" +
	    			 		"f2.favorite_check,\n" +
	    			 		"COUNT(f.id) favorite_count\n" +
	    			 		"FROM \n" +
	    			 		"user u\n" +
	    			 		"INNER JOIN\n" +
	    			 		"sound s\n" +
	    			 		"ON\n" +
	    			 		"u.user_id = s.user_id\n" +
	    			 		"INNER JOIN\n" +
	    			 		"animal a\n" +
	    			 		"ON\n" +
	    			 		"a.animal_id = u.animal_id\n" +
	    			 		"LEFT OUTER JOIN \n" +
	    			 		"favorite f\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f.sound_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"(SELECT count(*)\n" +
	    			 		" favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id  ) f2\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f2.sound_id\n" +
	    			 		"WHERE u.user_name LIKE ? \n" +
	    			 		"OR a.animal_name LIKE ? \n" +
	    			 		"OR s.sound_content LIKE ? \n" +
	    			 		"group by\n" +
	    			 		"s.sound_id Desc;\n"
	    			 		) ;
	    	 st.setInt(1,userId);
	    	 st.setString(2, "%" + searchWord + "%");
	    	 st.setString(3,  "%" + searchWord + "%");
	    	 st.setString(4,  "%" + searchWord + "%");

			ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	Date soundDate = rs.getDate("sound_date");
	        	int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }



	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg, animalLanguage,
	        				soundId,soundContent,soundDate,favoCheck,favoCount);
	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}


		public List<SoundInfoBeans> SearchAnimalSound(int userId,String searchWord) throws SQLException{
		Connection conn = null;
	    List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	    PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 			("SELECT\n" +
		    			 		"u.user_id,\n" +
		    			 		"u.user_name,\n" +
		    			 		"a.animal_id,\n" +
		    			 		"a.animal_img,\n" +
		    			 		"a.animal_language,\n" +
		    			 		"s.sound_id,\n" +
		    			 		"s.sound_content,\n" +
		    			 		"s.sound_date,\n" +
		    			 		"f2.favorite_check,\n" +
		    			 		"COUNT(f.id) favorite_count\n" +
		    			 		"FROM \n" +
		    			 		"user u\n" +
		    			 		"INNER JOIN\n" +
		    			 		"sound s\n" +
		    			 		"ON\n" +
		    			 		"u.user_id = s.user_id\n" +
		    			 		"INNER JOIN\n" +
		    			 		"animal a\n" +
		    			 		"ON\n" +
		    			 		"a.animal_id = u.animal_id\n" +
		    			 		"LEFT OUTER JOIN \n" +
		    			 		"favorite f\n" +
		    			 		"ON\n" +
		    			 		"s.sound_id = f.sound_id\n" +
		    			 		"LEFT OUTER JOIN\n" +
		    			 		"(SELECT count(*)\n" +
		    			 		" favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id  ) f2\n" +
		    			 		"ON\n" +
		    			 		"s.sound_id = f2.sound_id\n" +
		    			 		"WHERE u.user_name LIKE ? \n" +
		    			 		"OR a.animal_name LIKE ? \n" +
		    			 		"OR s.sound_content LIKE ? \n" +
		    			 		"group by\n" +
		    			 		"s.sound_id Desc;\n"
		    			 		) ;
		    	 st.setInt(1,userId);
		    	 st.setString(2, "%" + searchWord + "%");
		    	 st.setString(3,  "%" + searchWord + "%");
		    	 st.setString(4,  "%" + searchWord + "%");

			ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");



	        	 String animalLanguage = rs.getString("animal_language");



	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	Date soundDate = rs.getDate("sound_date");
	        	int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }



	        	//animalLanguageを用いて置換
	        	String str = "";
	        	if(soundContent.length() > animalLanguage.length()) {
		        	 int length =  soundContent.length()/animalLanguage.length();


		             int w = 0;
		             while(true){
		                 str += animalLanguage;
		                 w++;
		                 if(w == length){
		                     break;
		                 }
		             }


		             length = soundContent.length()%animalLanguage.length();

		             String[] strArray = new String[animalLanguage.length()];
		             for(int i = 0;i<animalLanguage.length();i++){
		                 String str3 = String.valueOf(str.charAt(i));
		                 strArray[i] = str3;
		             }

		             for (int i = 0; i < length; i++) {
		                str += strArray[i];
		                }



		        	}else {


		        		String[] strArray = new String[animalLanguage.length()];
			             for(int i = 0;i<animalLanguage.length();i++){
			                 String str3 = String.valueOf(animalLanguage.charAt(i));
			                 strArray[i] = str3;
			             }

			             for (int i = 0; i < soundContent.length(); i++) {
				                str += strArray[i];
				                }

		        	}

	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg, animalLanguage,
	        				soundId,str,soundDate,favoCheck,favoCount);

	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}

	public List<SoundInfoBeans> AllAnimalSound(int userId) throws SQLException{
		Connection conn = null;
	     List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	     PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 		("SELECT\n" +
	    			 		"u.user_id,\n" +
	    			 		"u.user_name,\n" +
	    			 		"a.animal_id,\n" +
	    			 		"a.animal_img,\n" +
	    			 		"a.animal_language,\n" +
	    			 		"s.sound_id,\n" +
	    			 		"s.sound_content,\n" +
	    			 		"s.sound_date,\n" +
	    			 		"f2.favorite_check,\n" +
	    			 		"COUNT(f.id) favorite_count\n" +
	    			 		"FROM \n" +
	    			 		"user u\n" +
	    			 		"INNER JOIN\n" +
	    			 		"sound s\n" +
	    			 		"ON\n" +
	    			 		"u.user_id = s.user_id\n" +
	    			 		"INNER JOIN\n" +
	    			 		"animal a\n" +
	    			 		"ON\n" +
	    			 		"a.animal_id = u.animal_id\n" +
	    			 		"LEFT OUTER JOIN \n" +
	    			 		"favorite f\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f.sound_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"(SELECT count(*)\n" +
	    			 		" favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id  ) f2\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f2.sound_id\n" +
	    			 		"group by\n" +
	    			 		"s.sound_id DESC;");

	    	 st.setInt(1, userId);
	    	 ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	 Date soundDate = rs.getDate("sound_date");
	        	 int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }

	        	 String str = "";
		        	if(soundContent.length() > animalLanguage.length()) {
			        	 int length =  soundContent.length()/animalLanguage.length();


			             int w = 0;
			             while(true){
			                 str += animalLanguage;
			                 w++;
			                 if(w == length){
			                     break;
			                 }
			             }


			             length = soundContent.length()%animalLanguage.length();

			             String[] strArray = new String[animalLanguage.length()];
			             for(int i = 0;i<animalLanguage.length();i++){
			                 String str3 = String.valueOf(str.charAt(i));
			                 strArray[i] = str3;
			             }

			             for (int i = 0; i < length; i++) {
			                str += strArray[i];
			                }



			        	}else {


			        		String[] strArray = new String[animalLanguage.length()];
				             for(int i = 0;i<animalLanguage.length();i++){
				                 String str3 = String.valueOf(animalLanguage.charAt(i));
				                 strArray[i] = str3;
				             }

				             for (int i = 0; i < soundContent.length(); i++) {
					                str += strArray[i];
					                }

			        	}


	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg, animalLanguage,
	        				soundId,str,soundDate,favoCheck,favoCount);

	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}

	public List<SoundInfoBeans> userAnimalSound(int animalId,int userId) throws SQLException{
		Connection conn = null;
	     List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	     PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 ("SELECT user.user_id,user.user_name,animal.animal_id,animal.animal_img,animal.animal_language,sound.sound_id,sound.sound_content,sound.sound_date FROM user INNER JOIN animal ON animal.animal_id = ? AND user.animal_id = ? INNER JOIN  sound ON sound.user_id = ? AND user.user_id = ? ORDER BY sound.sound_id DESC") ;

	    	 st.setInt(1, animalId);
	    	 st.setInt(2, animalId);
	    	 st.setInt(3, userId);
	    	 st.setInt(4, userId);
			ResultSet rs = st.executeQuery();




	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	Date soundDate = rs.getDate("sound_date");

	        	String str ="";
	        	if(soundContent.length() > animalLanguage.length()) {
	        	 int length =  soundContent.length()/animalLanguage.length();


	             int w = 0;
	             while(true){
	                 str += animalLanguage;
	                 w++;
	                 if(w == length){
	                     break;
	                 }
	             }


	             length = soundContent.length()%animalLanguage.length();

	             String[] strArray = new String[animalLanguage.length()];
	             for(int i = 0;i<animalLanguage.length();i++){
	                 String str3 = String.valueOf(str.charAt(i));
	                 strArray[i] = str3;
	             }

	             for (int i = 0; i < length; i++) {
	                str += strArray[i];
	                }



	        	}else {


	        		String[] strArray = new String[animalLanguage.length()];
		             for(int i = 0;i<animalLanguage.length();i++){
		                 String str3 = String.valueOf(animalLanguage.charAt(i));
		                 strArray[i] = str3;
		             }

		             for (int i = 0; i < soundContent.length(); i++) {
			                str += strArray[i];
			                }

	        	}


	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg, animalLanguage,
	        				soundId,str,soundDate);

	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}


	public List<SoundInfoBeans> userAnimalSound(int userId) throws SQLException{
		Connection conn = null;
	     List<SoundInfoBeans> userSoundList = new ArrayList<SoundInfoBeans>();
	     PreparedStatement st = null;
	     try {
	    	 conn = DBManager.getConnection();

	    	 st  = conn.prepareStatement
	    			 ("SELECT\n" +
	    			 		"u.user_id,\n" +
	    			 		"u.user_name,\n" +
	    			 		"a.animal_id,\n" +
	    			 		"a.animal_img,\n" +
	    			 		"a.animal_language,\n" +
	    			 		"s.sound_id,\n" +
	    			 		"s.sound_content,\n" +
	    			 		"s.sound_date,\n" +
	    			 		"f2.favorite_check,\n" +
	    			 		"COUNT(f.id) favorite_count\n" +
	    			 		"FROM \n" +
	    			 		"user u\n" +
	    			 		"INNER JOIN\n" +
	    			 		"sound s\n" +
	    			 		"ON\n" +
	    			 		"u.user_id = s.user_id\n" +
	    			 		"INNER JOIN\n" +
	    			 		"animal a\n" +
	    			 		"ON\n" +
	    			 		"a.animal_id = u.animal_id\n" +
	    			 		"LEFT OUTER JOIN \n" +
	    			 		"favorite f\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f.sound_id\n" +
	    			 		"LEFT OUTER JOIN\n" +
	    			 		"(SELECT count(*)\n" +
	    			 		" favorite_check ,sound_id FROM favorite WHERE user_id = ? GROUP BY sound_id  ) f2\n" +
	    			 		"ON\n" +
	    			 		"s.sound_id = f2.sound_id\n" +
	    			 		"where\n" +
	    			 		"u.user_id = ? \n" +
	    			 		"group by\n" +
	    			 		"s.sound_id DESC;") ;


	    	 st.setInt(1, userId);
	    	 st.setInt(2, userId);
			ResultSet rs = st.executeQuery();



	         while(rs.next()) {
	        	 int userid = rs.getInt("user_id");
	        	 String userName = rs.getString("user_name");
	        	 int animalid = rs.getInt("animal_id");
	        	 String animalImg = rs.getString("animal_img");
	        	 String animalLanguage = rs.getString("animal_language");
	        	 int soundId = rs.getInt("sound_id");
	        	 String soundContent = rs.getString("sound_content");
	        	Date soundDate = rs.getDate("sound_date");

	        	 int favoCheck;
	        	 int favoCount;

	        	 String s1 = rs.getString("favorite_check");
	        	 String s2 = rs.getString("favorite_count");

	        	 if(s1 != null) {
	        		favoCheck = Integer.parseInt(s1);
	        	 }else {
	        		 favoCheck = 0;
	        	 }

	        	 if(s2 != null) {
		        		favoCount = Integer.parseInt(s2);
		        	 }else {
		        		 favoCount = 0;
		        	 }

	        	 String str ="";
		        	if(soundContent.length() > animalLanguage.length()) {
		        	 int length =  soundContent.length()/animalLanguage.length();


		             int w = 0;
		             while(true){
		                 str += animalLanguage;
		                 w++;
		                 if(w == length){
		                     break;
		                 }
		             }


		             length = soundContent.length()%animalLanguage.length();

		             String[] strArray = new String[animalLanguage.length()];
		             for(int i = 0;i<animalLanguage.length();i++){
		                 String str3 = String.valueOf(str.charAt(i));
		                 strArray[i] = str3;
		             }

		             for (int i = 0; i < length; i++) {
		                str += strArray[i];
		                }



		        	}else {


		        		String[] strArray = new String[animalLanguage.length()];
			             for(int i = 0;i<animalLanguage.length();i++){
			                 String str3 = String.valueOf(animalLanguage.charAt(i));
			                 strArray[i] = str3;
			             }

			             for (int i = 0; i < soundContent.length(); i++) {
				                str += strArray[i];
				                }

		        	}



	        	 SoundInfoBeans sound = new SoundInfoBeans(userid,userName, animalid,animalImg, animalLanguage,
	        				soundId,str,soundDate,favoCheck,favoCount);


	        	 userSoundList.add(sound);

	         }
	     }catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return userSoundList;
	}

}
