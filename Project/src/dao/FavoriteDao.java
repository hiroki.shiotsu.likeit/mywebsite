package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class FavoriteDao {

	public int AddFavorite(int userId,int soundId ) throws SQLException{
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;

		try {

	   		 conn = DBManager.getConnection();

	   		 String sql = "INSERT INTO favorite (user_id,sound_id) VALUES (?,?)";
			 pStmt = conn.prepareStatement(sql);

			 pStmt.setInt(1, userId);
	         pStmt.setInt(2,soundId);

	         int result = pStmt.executeUpdate();

	         if(result > 0) {
				 System.out.println("Success");
				 return success;
			 }else {
				return failed;
			 }
	}catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (conn != null) {
			conn.close();

		}
	}


}


	public int DeleteFavorite(int userId,int soundId ) throws SQLException{
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;

		try {

	   		 conn = DBManager.getConnection();

	   		 String sql = "DELETE FROM favorite WHERE user_id = ? AND sound_id = ?";

			 pStmt = conn.prepareStatement(sql);

			 pStmt.setInt(1, userId);
	         pStmt.setInt(2,soundId);

	         int result = pStmt.executeUpdate();

	         if(result > 0) {
				 System.out.println("Success");
				 return success;
			 }else {
				return failed;
			 }
	}catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (conn != null) {
			conn.close();

		}
	}


}



}
