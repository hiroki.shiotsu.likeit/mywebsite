package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.AnimalDataBeans;



public class AnimalDao {

	public int CreateAnimal(String animalName,String animalImg ,String animalLanguage,String animalSound) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;

		try {
			conn = DBManager.getConnection();
			 String sql ="INSERT INTO animal (animal_name,animal_img,animal_language,animal_sound) VALUES (?,?,?,?)";

			 pStmt = conn.prepareStatement(sql);

			 pStmt.setString(1, animalName);
	         pStmt.setString(2,animalImg);
	         pStmt.setString(3,animalLanguage);
	         pStmt.setString(4,animalSound);

	         int result = pStmt.executeUpdate();

	         if(result > 0) {
				 System.out.println("Success");
				 return success;
			 }else {
				return failed;
			 }
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}

	}


	//ユーザーから取得したアニマルIDを利用し、情報を取得する。
	public static AnimalDataBeans getAnimal(int animalId) throws SQLException {
		AnimalDataBeans ab = new AnimalDataBeans();
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();

			st = conn.prepareStatement("SELECT * FROM animal WHERE animal_id = ?");
			st.setInt(1, animalId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ab.setAnimalId(rs.getInt("animal_id"));
				ab.setAnimalName(rs.getString("animal_name"));
				ab.setAnimalImg(rs.getString("animal_img"));
				ab.setAnimalLanguage(rs.getString("animal_language"));
				ab.setAnimalSound(rs.getString("animal_sound"));
			}


			System.out.println("searching AnimalData  has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return ab;


	}

	//アニマルテーブルを全て読み込むDAO
	public List<AnimalDataBeans> allAnimal() throws SQLException {
		 Connection conn = null;
	     List<AnimalDataBeans> animalList = new ArrayList<AnimalDataBeans>();

	     try {
	    	 conn = DBManager.getConnection();

	    	 String sql = "SELECT * FROM  animal";

	    	 Statement stmt = conn.createStatement();
	         ResultSet rs = stmt.executeQuery(sql);

	         while (rs.next()) {
	                int animalId = rs.getInt("animal_id");
	                String animalName = rs.getString("animal_name");
	                String animalImg = rs.getString("animal_img");
	                String animalLanguage = rs.getString("animal_language");
	                String animalSound = rs.getString("animal_sound");
	                AnimalDataBeans animal = new AnimalDataBeans(animalId,animalName,animalImg,animalLanguage,animalSound);

	                animalList.add(animal);
	            }

	     } catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (conn != null) {
					conn.close();
				}
			}
	     return animalList;
	}

	public int DeleteAnimal(int animalId) throws SQLException {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int success = 1;
		int failed = 0;

		try {
			conn = DBManager.getConnection();
			 String sql ="DELETE FROM animal WHERE animal_id = ?";

			 pStmt = conn.prepareStatement(sql);

			 pStmt.setInt(1, animalId);

	         int result = pStmt.executeUpdate();

	         if(result > 0) {
				 System.out.println("Success");
				 return success;
			 }else {
				return failed;
			 }
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();

			}
		}

	}



}
