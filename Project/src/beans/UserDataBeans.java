package beans;

import java.io.Serializable;
import java.util.Date;


public class UserDataBeans implements Serializable {
	private int userId;
	private String loginId;
	private String password;
	private String userName;
	private Date birthDate;
	private int animalId;
	private String userIntroduction;
	private Date createDate;

	//コンストラクタ
	public UserDataBeans() {
		this.loginId = "";
		this.password = "";
		this.userName = "";
		this.birthDate = null;
		this.animalId = 1;
		this.userIntroduction = "";
	}



	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public int getAnimalId() {
		return animalId;
	}
	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}
	public String getUserIntroduction() {
		return userIntroduction;
	}
	public void setUserIntroduction(String userIntroduction) {
		this.userIntroduction = userIntroduction;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




}

