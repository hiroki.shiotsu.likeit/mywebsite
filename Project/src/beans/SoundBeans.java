package beans;

import java.io.Serializable;
import java.sql.Date;


public class SoundBeans implements Serializable {
	private int soundId;
	private int userId;
	private String soundContent;
	private Date soundDate;

	public SoundBeans() {
		this.soundId = 0;
		this.userId = 0;
		this.soundContent = null;
		this.soundDate = null;
	}

	//サウンドデータ取得のためのコンストラクタ
	public SoundBeans(int soundId,int userId,String soundContent,Date soundDate) {
		this.soundId = soundId;
		this.userId = userId;
		this.soundContent = soundContent;
		this.soundDate = soundDate;
	}


	public int getSoundId() {
		return soundId;
	}

	public void setSoundId(int soundId) {
		this.soundId = soundId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getSoundContent() {
		return soundContent;
	}

	public void setSoundContent(String soundContent) {
		this.soundContent = soundContent;
	}

	public Date getSoundDate() {
		return soundDate;
	}

	public void setSoundDate(Date soundDate) {
		this.soundDate = soundDate;
	}



}

