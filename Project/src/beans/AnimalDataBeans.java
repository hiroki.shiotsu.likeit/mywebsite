package beans;

import java.io.Serializable;


public class AnimalDataBeans implements Serializable {
	private int animalId;
	private String animalName;
	private String animalImg;
	private String animalLanguage;
	private String animalSound;

	public AnimalDataBeans() {
		this.animalId = 0;
		this.animalName = "";
		this.animalImg = "";
		this.animalLanguage = "";
		this.animalSound = "";
 	}


	//全アニマル取得に使うコンストラクタ
	public AnimalDataBeans(int animalId,String animalName,
			String animalImg, String animalLanguage, String animalSound) {

		this.animalId = animalId;
		this.animalName = animalName;
		this.animalImg = animalImg;
		this.animalLanguage = animalLanguage;
		this.animalSound = animalSound;

	}

	public int getAnimalId() {
		return animalId;
	}

	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getAnimalImg() {
		return animalImg;
	}

	public void setAnimalImg(String animalImg) {
		this.animalImg = animalImg;
	}

	public String getAnimalLanguage() {
		return animalLanguage;
	}

	public void setAnimalLanguage(String animalLanguage) {
		this.animalLanguage = animalLanguage;
	}

	public String getAnimalSound() {
		return animalSound;
	}

	public void setAnimalSound(String animalSound) {
		this.animalSound = animalSound;
	}

}
