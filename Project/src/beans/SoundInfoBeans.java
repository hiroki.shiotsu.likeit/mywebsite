package beans;

import java.io.Serializable;
import java.sql.Date;



public class SoundInfoBeans implements Serializable {
	private int userId;
	private String userName;
	private int animalId;
	private String animalImg;
	private String animalLanguage;
	private int soundId;
	private String soundContent;
	private Date soundDate;
	private int favoCheck;
	private int favoCount;


	public String getAnimalLanguage() {
		return animalLanguage;
	}


	public void setAnimalLanguage(String animalLanguage) {
		this.animalLanguage = animalLanguage;
	}


	public int getFavoCheck() {
		return favoCheck;
	}


	public void setFavoCheck(int favoCheck) {
		this.favoCheck = favoCheck;
	}


	public int getFavoCount() {
		return favoCount;
	}


	public void setFavoCount(int favoCount) {
		this.favoCount = favoCount;
	}


	public SoundInfoBeans(int userId,String userName, int animalId,String animalImg, String animalLanguage,
			int soundId,String soundContent,Date soundDate){
		this.userId = userId;
		this.userName = userName;
		this.animalId = animalId;
		this.animalImg = animalImg;
		this.animalLanguage = animalLanguage;
		this.soundId = soundId;
		this.soundContent = soundContent;
		this.soundDate = soundDate;
	}

	public SoundInfoBeans(int userId,String userName, int animalId,String animalImg, String animalLanguage,
			int soundId,String soundContent,Date soundDate,int favoCheck,int favoCount){
		this.userId = userId;
		this.userName = userName;
		this.animalId = animalId;
		this.animalImg = animalImg;
		this.animalLanguage = animalLanguage;
		this.soundId = soundId;
		this.soundContent = soundContent;
		this.soundDate = soundDate;
		this.favoCheck = favoCheck;
		this.favoCount = favoCount;
	}


	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getAnimalId() {
		return animalId;
	}
	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}
	public String getAnimalImg() {
		return animalImg;
	}
	public void setAnimalImg(String animalImg) {
		this.animalImg = animalImg;
	}
	public String getAnimal_language() {
		return animalLanguage;
	}
	public void setAnimal_language(String animal_language) {
		this.animalLanguage = animal_language;
	}
	public int getSoundId() {
		return soundId;
	}
	public void setSoundId(int soundId) {
		this.soundId = soundId;
	}
	public String getSoundContent() {
		return soundContent;
	}
	public void setSoundContent(String soundContent) {
		this.soundContent = soundContent;
	}
	public Date getSoundDate() {
		return soundDate;
	}
	public void setSoundDate(Date soundDate) {
		this.soundDate = soundDate;
	}


}

