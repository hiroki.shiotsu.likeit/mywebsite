<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>アニマル共和国</title>
    <link rel="stylesheet" href="layout/css/animal-land.css" >
    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" href="layout/css/editUserInfo.css">
</head>
<body>
   <nav class="navbar navbar-light justify-content-between">
  <a class="navbar-logo" href="mypage">AnimalLand</a>

     <form class="form-inline" action="Logout" method="get">
            <!-- 切り替えボタンの設定 -->
       <div class="index-nav-form">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
          ログアウト
        </button>
        </div>
        <!-- モーダルの設定 -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog">
            <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ログアウト</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>本当に人間界に帰りますか？</p>
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn" data-dismiss="modal">帰らない</button>


        <button type="submit" class="btn btn-danger">ログアウト</button>

      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!--　モータルの処理　-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
     </form>
    </nav>


   <div class="container">
     <div class="edit-layout">
    <div class="layout-center">
        <h1 class="page-title">ユーザー情報の編集</h1>
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="color: red;">
		  ${errMsg}
		</div>
		</c:if>

        <form action="EditUserInfo" method="post">

        <div class="login-form">
            <p>ログインID</p>
            <p>${userInfo.loginId}</p>
            <input type="hidden" name="loginId" value="${userInfo.loginId}">
        </div>

        <div class="login-form">
            <p>パスワード</p>
            <input type="password" name="password1" style="width: 96%;">
        </div>

        <div class="login-form">
            <p>パスワード(確認)</p>
            <input type="password" name="password2" style="width: 96%;">
        </div>

        <div class="login-form">
            <p>ユーザー名</p>
            <input type="text" value="${userInfo.userName}" name="userName" style="width: 96%;">
        </div>

        <div class="login-form">
            <p>生年月日</p>
            <input type="date" value="${userInfo.birthDate}" name="birthDate" style="width: 96%;">
        </div>

        <div class="login-form">
            <p>アニマルの変更</p>
            <select class="form-control" id="number" name="animalId">
            <c:forEach var="animal" items="${animalList}" >
            <option value="${animal.animalId}">${animal.animalName}</option>
            </c:forEach>

            </select>
        </div>




         <div class="entry-btn">
                <button class="btn" type="submit">登録</button>
        </div>
        </form>
	<c:if test="${userId != 1}">
	<div class="back">
		<a href="Delete" style="color: white" onMouseOut="this.style.color='white'" onMouseOver="this.style.color='red'">アカウントを削除する</a>
		</div>
	</c:if>


    </div>
    </div>







</div>

</body>
</html>