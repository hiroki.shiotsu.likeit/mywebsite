<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>新規登録</title>
    <link rel="stylesheet" href="layout/css/animal-land.css" >
    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="layout/css/newAnimal.css">
</head>
<body>
<div class="container">
    <div class="new-animal-layout">
    <div class="layout-center">



           <div class="login-form-logo">
            <img src="img/logorilla.png" alt="" width="50px" height="50px">
             </div>

        <h1 class="new-animal-title">新規登録</h1>

		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="color: red;">
		  ${errMsg}
		</div>
		</c:if>

        <form  action="CreateUser" method="post">

        <div class="login-form">
            <p>ログインID</p>
            <input type="text" name="loginId" value="${loginId}"><br>
        </div>

        <div class="login-form">
            <p>パスワード</p>
            <input type="password" name="password1">
        </div>

        <div class="login-form">
            <p>パスワード(確認)</p>
            <input type="password" name="password2">
        </div>

        <div class="login-form">
            <p>ユーザー名</p>
            <input type="text" name="userName" value="${userName}">
        </div>

        <div class="login-form">
            <p>生年月日</p>
            <input type="date" name="birthDate" value="${birthDate}">
        </div>

        <div class="login-form">
            <p>どのアニマルになる？</p>
            <select class="form-control" id="number" name="animalId">
            <c:forEach var="animal" items="${animalList}" >
            <option value="${animal.animalId}">${animal.animalName}</option>
            </c:forEach>

            </select>
        </div>

        <div class="entry-btn">
                <button class="btn" type="submit">登録</button>
        </div>
        </form>

        <div class="back">
            <a href="Index">トップへ</a>
        </div>

    </div>
     </div>


</div>
</body>
</html>