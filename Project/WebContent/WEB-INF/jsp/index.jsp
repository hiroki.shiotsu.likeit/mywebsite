<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>アニマル共和国</title>

    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
     <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
   <link rel="stylesheet" href="layout/css/title.css" type="text/css">

</head>
<body>
    <div>
        <nav class="navbar navbar-light  justify-content-between">
         <div class="navbar-logo"><a href="#">AnimalLand</a></div>



               <div class="index-nav-form">
                 <form action="CreateUser"> <button class="btn btn-light" type="submit">新規登録</button></form>
                 <form action="Login"><button class="btn btn-primary login-btn" type="submit">ログイン</button></form>
               </div>

        </nav>
        </div>
    <div class="container">
       <div class="row login-container">
           <div class="col1 col-md-6">
           <div class="title">
               <h1 class="title animate__animated animate__backInLeft">Animal <br> Land</h1>
           </div>

        </div>
        <div class="col2 col-md-6 animate__animated animate__fadeIn animate__delay-1s">

            <h2 class="col2-title">人間を忘れて　飛び込もう。</h2>

         <form action="Login" method="post">
               <div class="login-box">
                <div class="login-form">
                    <p>ログインID</p>
                    <input type="text" name="loginId"><br>
                </div>

                <div class="login-form">
                    <p>パスワード</p>
                    <input type="password" name="password">
                </div>

                <div class="login-page-btn">
                    <button class="btn btn-primary login-btn" type="submit">ログイン</button>
                </div>
                </div>
            </form>

                <div class="form-select"><b>or</b></div>


		<form action="CreateUser" method="get">
            <div class="form-button-i">
                 <button class="btn btn-light new-btn" type="submit">新規登録</a></button>
            </div>
		</form>

        </div>
       </div>

    </div>



</body>
</html>