<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>アニマル共和国</title>
    <link rel="stylesheet" href="layout/css/animal-land.css" >
    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="layout/css/createAnimal.css">
</head>
<body>
   <nav class="navbar navbar-light justify-content-between">
      <a class="navbar-logo" href="mypage">AnimalLand</a>

     <form class="form-inline" action="Logout" method="get">
            <!-- 切り替えボタンの設定 -->
       <div class="index-nav-form">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
          ログアウト
        </button>
        </div>
<!-- モーダルの設定 -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ログアウト</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>本当に人間界に帰りますか？</p>
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn" data-dismiss="modal">帰らない</button>


        <button type="submit" class="btn btn-danger">ログアウト</button>

      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
     </form>
    </nav>

    <div class="container">

        <div class="create-animal">


          <div class="btn-center">
            <!-- 切り替えボタンの設定 -->
            <button type="button" class="btn　btn-primary" data-toggle="modal" data-target="#Modal">
                  動物の追加
            </button>
         </div>
            <!-- モーダルの設定 -->

            <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
              <!--以下modal-dialogのCSSの部分で modal-lgやmodal-smを追加するとモーダルのサイズを変更することができる-->
              <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="Modal">動物の追加</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
                  <span aria-hidden="true">&times;</span>
                </button>
                    </div>
                <form action="CreateAnimal" method="post">
              <div class="modal-body">

               <!-- form -->


        <div class="login-form-i">
            <p>動物名</p>
            <input type="text" name="animalName" value="${animalName}">
        </div>

        <div class="login-form-i">
            <p>動物のアイコン</p>
            <input type="text" name="animalImg" value="${animalImg}">
        </div>

        <div class="login-form-i">
            <p>動物言語</p>
            <input type="text" name="animalLanguage" value="${animalLanguage}">
        </div>

		<div class="login-form-i">
            <p>動物の鳴き声(つぶやき用)</p>
            <input type="text" name="animalSound" value="${animalSound}" maxlength="6">
        </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                <button type="submit" class="btn btn-primary">保存</button>
              </div>
                </form>
                </div>
                  </div>
             </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>




          <div class="animal-list-title"><b>動物一覧</b></div>





	<div class="animal-list">
	<c:forEach var="animal" items="${animalList}" >
	<form action="AnimalDelete" method="get">
	<input type="hidden" value="${animal.animalId}" name="animalId">
		 <div class="animal-list-layout">
               <div class="animal-list-content">
                <img width="100px" height="100px" src="layout/img/${animal.animalImg}" alt="#">
                 <div class="animal-list-text">
                  <p><span>動物名 : ${animal.animalName}</span></p>
                  <p>動物語 : ${animal.animalLanguage}</p>
                </div>
                </div>
         <div class="animal-delete">
            <button class="animal-Delete"type="button" data-toggle="modal" data-target="#Modal${animal.animalId}">
                ☓
            </button>
            </div>
        <!-- モーダルの設定 -->
        <div class="modal fade" id="Modal${animal.animalId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">動物の消去</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>動物を削除しますか？<p>
          </div>
          <div class="modal-footer ">
            <button type="button" class="btn" data-dismiss="modal">いいえ</button>


            <button type="submit" class="btn btn-danger">はい</button>

          </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->




              </div>
              </form>
   			</c:forEach>
			</div>

        </div>



    </div>



</body>