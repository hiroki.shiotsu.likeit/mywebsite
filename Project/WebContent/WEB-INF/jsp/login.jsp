<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="layout/css/animal-land.css" type="text/css">
    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="layout/css/login.css" type="text/css">
</head>
<body>
  <div class="container">
  <div class="login-page-layout">
   <div class="layout-center">

        <div class="login-form-logo">
            <img src="layout/img/logorilla.png" alt="" width="50px" height="50px">
        </div>

		<div class="login-title">
        <h1>
            アニマル共和国に入国
       </h1>
      </div>
      
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="color: red;">
		  ${errMsg}
		</div>
		</c:if>
            <form action="Login" method="post">
                <div class="login-form">
                    <p>ログインID</p>
                    <input type="text" name="loginId"><br>
                </div>

                <div class="login-form">
                    <p>パスワード</p>
                    <input type="password" name="password">
                </div>

                <div class="login-page-btn">
                    <button class="btn btn-primary" type="submit">ログイン</button>
                </div>

            </form>

        <div class="account-redirect"><a href="newAnimal.html">アカウント作成</a></div>

   </div>
   </div>
</div>
</body>
</html>