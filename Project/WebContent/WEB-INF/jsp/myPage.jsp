<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>マイページ</title>
    <link rel="stylesheet" href="layout/css/animal-land.css" >
    <link rel="stylesheet" href="layout/css/mypage.css">
    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-light justify-content-between">

  <a class="navbar-logo" href="#">AnimalLand</a>

     <form class="form-inline" action="Logout" method="get">
           <!-- 切り替えボタンの設定 -->
       <div class="index-nav-form">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
          ログアウト
        </button>
        </div>
        <!-- モーダルの設定 -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog">
            <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ログアウト</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>本当に人間界に帰りますか？</p>
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn" data-dismiss="modal">帰らない</button>


		<input type="hidden" name="1">
        <button type="submit" class="btn btn-danger">ログアウト</button>


      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!--　モータルの処理　-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

     </form>

</nav>
<div class="navbar-inner"></div>

  <div class="container">

    <div class="row">
     <div class="sound col-lg-4">
        <form action="Search" method="post">
            <input class="form-control" type="text" placeholder="検索" aria-label="Search" style="width: 100%;" name="searchWord">
        </form>



        <div class="user-info">
            <div class="user-avater">
              <img width="60" height="60" src="layout/img/${animalInfo.animalImg}" alt="">
           </div>

           <div class="user-name">
               <span>ユーザー名</span><br>
               <strong>${userInfo.userName}</strong><br>
               <span>動物名 :${animalInfo.animalName} </span>
           </div>

           <div class="icons">
            <span class="material-icons"><a href="EditUserInfo" style="color: black;">settings</a></span>
			<c:if test="${userId == 1}">

		 	 <span class="material-icons"><a href="CreateAnimal" style="color: black;">pets</a></span>

			</c:if>


           </div>

        </div>

        <div class="sound-form">
            <form action="Sound" method="post">
            	<input type="hidden" name="userId" value="${userId}">
            	<input type="hidden" name="flg" value="2">
                <textarea cols="30" rows="10"  placeholder="今の気持ちを鳴こう!" name="soundContent"></textarea>


                <button class="btn  sound-btn" type="submit" style="border-radius: 50px;"><b>${animalInfo.animalSound}</b></button>
            </form>
        </div>



    </div>


     <!-- プロフィール-->
    <div class="profile col-lg-7" style="padding: 0px;">
				<div class="return">
					<a href="TimeLine">←戻る</a>
					<div class="btn-group change-btn" role="group"
						aria-label="Basic example">
						<form action="mypage" method="get">
							<button type="submit" class="btn btn-secondary">人間語</button>
						</form>
						<form action="AnimalLanguage" method="get">
							<button type="submit" class="btn btn-secondary">動物語</button>
							<input type="hidden" name="USL" value="${searchWord}"> <input
								type="hidden" name="flg" value="1">
						</form>
					</div>
				</div>

				<div class="profile-box">
           <div class="profile-content">
               <div class="profile-icon">
                   <img width="140px" height="140px" src="layout/img/${animalInfo.animalImg}" alt="#">
               </div>

               <div class="profile-name">
                   <b>${userInfo.userName}</b><br>
                   <span>動物名 : ${animalInfo.animalName}</span><br>
                   <span>生年月日 : ${userInfo.birthDate}</span>
               </div>






            <!-- 切り替えボタンの設定 -->
            <div class="profile-edit">
            <button type="button" class="btn　btn-primary" data-toggle="modal" data-target="#Modal">
                 プロフィールの編集
            </button>
              </div>
            <!-- モーダルの設定 -->

            <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
              <!--以下modal-dialogのCSSの部分で modal-lgやmodal-smを追加するとモーダルのサイズを変更することができる-->
              <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="Modal">プロフィールの編集</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
                  <span aria-hidden="true">&times;</span>
                </button>
                    </div>
               <form action="EditProfile" method="post">
              <div class="modal-body">

               <!-- form -->


        <div class="login-form-i">
            <p>ユーザー名</p>
            <input type="text" name="userName">
        </div>


         <div class="login-form">
            <p>自己紹介</p>
            <textarea name="uI" id="" cols="30" rows="10"></textarea>
        </div>



              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                <button type="submit" class="btn btn-primary">保存</button>
              </div>
                </form>
                </div>
                  </div>
             </div>






           </div>

           <div class="profile-text">
               <p>${userInfo.userIntroduction}</p>

           </div>

           <div class="profile-user-info">
                <div class="all-sounds">
                	<span>鳴いた回数 : ${UNOT}回</span>

				<c:choose>
					<c:when test="${listCheck == 0}">
						<a href="mypage">自分の呟き</a>
					</c:when>
					<c:otherwise>
						<a href="FavoriteList?flg=1">お気に入り</a>
　					</c:otherwise>
				</c:choose>


                </div>

           </div>

       </div>

		<c:forEach var="usl" items="${USL}" >
		<div class="tl-hover">
           <div class="time-line">

           <div class="status-avater">
              <a href="UserPage?userId1=${usl.userId}"><img width="48" height="48" src="layout/img/${usl.animalImg}" alt="#" ></a>
           </div>

           <div class="status-info">
             <div class="sound-date">${usl.soundDate}</div>
              <strong>${usl.userName}</strong>

            <div class="status-text">
               <p>${usl.soundContent}</p>
            </div>
           </div>



       </div>
       <div class="favo-line">
       			<c:if test="${userId == usl.userId}">
				<a href="SoundDelete?soundId=${usl.soundId}">${animalInfo.animalSound}の削除</a>
				</c:if>
                <c:choose>
  				<c:when test="${usl.favoCheck == 1}">
  				<a href="Favorite?soundId=${usl.soundId}&flg=4" style="color:rgb(237,185,24);"><span class="material-icons">star</span></a>
  				<span class="count-favo">${usl.favoCount}</span>
 				</c:when>
  				<c:otherwise>
				<a href="Favorite?soundId=${usl.soundId}&flg=3" style="color:gray;"><span class="material-icons">star_border</span></a>
  				<span class="count-favo">${usl.favoCount}</span>
  				</c:otherwise>
				</c:choose>

          </div>
          </div>
	</c:forEach>

    </div>



     </div>
    </div>




</body>
</html>