<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>アカウント消去確認</title>
    <link rel="stylesheet" href="layout/css/animal-land.css" >
    <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="layout/css/delete.css">
</head>
<body>
     <nav class="navbar navbar-light justify-content-between">
  <a class="navbar-logo" href="#">AnimalLand</a>

     <form class="form-inline" action="Logout" method="get" >
          <!-- 切り替えボタンの設定 -->
       <div class="index-nav-form">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
          ログアウト
        </button>
        </div>
        <!-- モーダルの設定 -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog">
            <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ログアウト</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>本当に人間界に帰りますか？</p>
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn" data-dismiss="modal">帰らない</button>


        <button type="submit" class="btn btn-danger">ログアウト</button>

      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!--　モータルの処理　-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
     </form>
    </nav>




  <div class="container">

  <div class="login-page-layout">
   <div class="layout-center">

          <div class="delete-logo">
            <img src="layout/img/logorilla.png" alt="" width="50px" height="50px">
        </div>

        <h1 class="page-title" style="color: white;">アカウントの削除</h1>
        <p style="color: white;">本当に削除しますか？</p>

        <form action="Delete" method="post">
            <div class="delete-btn">
            <button type="submit" class="btn btn-light" type="submit" style="color: red;">削除する</button>
            <a href="mypage" class="btn btn-primary" type="submit" style="color: white;">削除しない</a>
            </div>
        </form>


   </div>
  </div>
</div>
</body>
</html>